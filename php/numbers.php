<?php
class HappyNumbers {
	private $number;
	
	public function __construct($number) {
		$this->number = $number;
	}
	
	private function isHappy($n) {
		while (1) {
	        	$total = 0;
			$past[] = 0;
			while ($n > 0) {
				$total += pow(($n % 10), 2);
				$n /= 10;
			}
			if ($total == 1)
				return true;
			if (array_key_exists($total, $past))
				return false;
			$n = $total;
			$past[$total] = 0;
		}
	}
	
	public function toString() {
		$i = $cnt = 0;
		while ($cnt < $this->number) {
		    if ($this->isHappy($i)) {
		        echo "$i ";
		        $cnt++;
		    }
		    $i++;
		}
	}
}
?>
